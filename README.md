# 1 Project .net core

## 1.1 Structure .net core

```
📦 Project
┣ 📂 Common
┃ ┣ 📂 Constans
┃ ┗ 📂 Utilities
┃
┣ 📂 DataAccess
┃ ┣ 📂 ModelDTOs
┃ ┣ 📂 Entities
┃ ┣ 📂 Repository
┃ ┃ ┗ 📂 Interface
┃ ┣ 📂 UnitOfWork
┃ ┃ ┗ 📂 Interface
┃ ┗ 📃 Context.cs
┃
┣ 📂 BusinessLogic
┃ ┣ 📂 Auth
┃ ┃ ┣ 📂 Interface
┃ ┃ ┗ 📃 AuthenticationHandle.cs
┃ ┗ 📂 Services
┃   ┗ 📂 Master
┃     ┗ 📂 Interface
┃
┗ 📂 API
  ┣ 📂 Controller
  ┃ 📃 appsetting.json
  ┗ 📃 Program.cs

```
## 1.2. Class file naming
Class names are written in [PascalCase].
Example: `MasterController`, `MasterService`, `MasterModel`, `MasterDTOs`.

# 2 Project nestJS

## 2.1 Structure nestJS

```
📦 Project
┣ 📂 src
┃ ┣ 📂 auth
┃ ┃ ┣ 📂 guards
┃ ┃ ┗ 📂 strategies
┃ ┣ 📂 helper
┃ ┣ 📂 modules
┃ ┃ ┗ 📂 master
┃ ┃   ┣ 📂 dto
┃ ┃   ┣ 📂 entities
┃ ┃   ┣ 📃 master.controller.ts
┃ ┃   ┣ 📃 master.module.ts
┃ ┃   ┗ 📃 master.service.ts
┃ ┣ 📃 app.module.ts
┃ ┗ 📃 main.ts
┣ 📃 .env
┗ 📃 package.json

```

## 2.2 Class file naming
Class names are written in [CamelCase].
Example: `masterData`, `masterDTOs`, `masterModel`.

