﻿using System.ComponentModel.Design;
using System.Text.RegularExpressions;

public class TestApp
{


    public static void Main(string[] args)
    {

        while (true)
        {
            Console.WriteLine("========= Choose your choice =========");
            Console.WriteLine("1 : Permutations \n2 : Find the odd int \n3 : Count the smiley faces!\n");
            Console.Write("Enter number choice : ");

            string choice = Console.ReadLine();

            if (choice == "1")
            {
                Console.WriteLine("========= Permutations =========\n");

                Console.Write("Use template input (y/n) : ");
                string input = Console.ReadLine();
                if (input.ToLower() == "y")
                {
                    string input1 = "a";
                    string input2 = "ab";
                    string input3 = "abc";
                    string input4 = "aabb";

                    Console.WriteLine(" '" + input1 + "'" + " Answer is : " + string.Join(", ", GeneratePermutations(input1)));
                    Console.WriteLine(" '" + input2 + "'" + " Answer is : " + string.Join(", ", GeneratePermutations(input2)));
                    Console.WriteLine(" '" + input3 + "'" + " Answer is : " + string.Join(", ", GeneratePermutations(input3)));
                    Console.WriteLine(" '" + input4 + "'" + " Answer is : " + string.Join(", ", GeneratePermutations(input4)));
                }
                else
                {
                    Console.Write("Enter input : ");
                    string inputData = Console.ReadLine();
                    Console.WriteLine(" '" + inputData + "'" + " Answer is : " + string.Join(", ", GeneratePermutations(inputData)));
                }
                Console.WriteLine("====================================\n\n");
            }
            else if (choice == "2")
            {
                Console.WriteLine("========= Find the odd int =========");
                Console.Write("Use template input (y/n) : ");
                string input = Console.ReadLine();
                if (input.ToLower() == "y")
                {

                    int[] input1 = new int[] { 7 };
                    int[] input2 = new int[] { 0 };
                    int[] input3 = new int[] { 1, 1, 2 };
                    int[] input4 = new int[] { 0, 1, 0, 1, 0 };
                    int[] input5 = new int[] { 1, 2, 2, 3, 3, 3, 4, 3, 3, 3, 2, 2, 1 };


                    Console.WriteLine("[" + string.Join(", ", input1) + "] Answer is : " + string.Join(", ", FindOddInt(input1)));
                    Console.WriteLine("[" + string.Join(", ", input2) + "] Answer is : " + string.Join(", ", FindOddInt(input2)));
                    Console.WriteLine("[" + string.Join(", ", input3) + "] Answer is : " + string.Join(", ", FindOddInt(input3)));
                    Console.WriteLine("[" + string.Join(", ", input4) + "] Answer is : " + string.Join(", ", FindOddInt(input4)));
                    Console.WriteLine("[" + string.Join(", ", input5) + "] Answer is : " + string.Join(", ", FindOddInt(input5)));
                }
                else
                {
                    Console.Write("Enter input (ex: 1,3) : ");
                    string inputData = Console.ReadLine();
                    int[] inputArr = Array.ConvertAll(inputData.Split(','), int.Parse);
                    Console.WriteLine("[" + string.Join(", ", inputArr) + "] Answer is : " + string.Join(", ", FindOddInt(inputArr)));
                }
                Console.WriteLine("====================================\n\n");


            }
            else if (choice == "3")
            {
                Console.WriteLine("========= Count the smiley faces! =========");
                Console.Write("Use template input (y/n) : ");
                string input = Console.ReadLine();
                if (input.ToLower() == "y")
                {

                    string[] input1 = new string[] { ":)", ";(", ";}", ":-D", ";D" };
                    string[] input2 = new string[] { ";D", ":-(", ":-)", ";~)" };
                    string[] input3 = new string[] { ";]", ":[", ";*", ":$", ";-D" };


                    Console.WriteLine("[ " + string.Join(" , ", input1) + " ] Answer is : " + string.Join(", ", CountSmileys(input1)));
                    Console.WriteLine("[ " + string.Join(" , ", input2) + " ] Answer is : " + string.Join(", ", CountSmileys(input2)));
                    Console.WriteLine("[ " + string.Join(" , ", input3) + " ] Answer is : " + string.Join(", ", CountSmileys(input3)));
                }
                else
                {
                    Console.Write("Enter input (ex: :),;D ) : ");
                    string inputData = Console.ReadLine();
                    Console.WriteLine("[" + string.Join(", ", inputData) + "] Answer is : " + string.Join(", ", CountSmileys(inputData.Split(','))));
                }
                Console.WriteLine("====================================\n\n");
            }
            else
            {
                Console.WriteLine("========= Choice not found =========");
            }


        }

    }


    public static List<string> GeneratePermutations(string input)
    {
        List<string> permutations = new List<string>();
        GeneratePermutationsRecursive(input.ToCharArray(), 0, input.Length - 1, permutations);
        return permutations;
    }

    private static void GeneratePermutationsRecursive(char[] arr, int left, int right, List<string> permutations)
    {
        if (left == right)
        {
            var checkDup = permutations.Where(q => q == new string(arr)).ToList();
            if (checkDup.Count == 0)
            {
                permutations.Add(new string(arr));
            }
        }
        else
        {
            for (int i = left; i <= right; i++)
            {
                Swap(ref arr[left], ref arr[i]);
                GeneratePermutationsRecursive(arr, left + 1, right, permutations);
                Swap(ref arr[left], ref arr[i]);
            }
        }
    }

    private static void Swap(ref char a, ref char b)
    {
        char temp = a;
        a = b;
        b = temp;
    }

    public static List<int> FindOddInt(int[] arr)
    {
        var oddInt = arr.GroupBy(q => q)
                     .Where(group => group.Count() % 2 == 1)
                     .Select(group => group.Key).ToList();

        return oddInt;
    }

    public static int CountSmileys(string[] arr)
    {
        string pattern = @"[:;][-~]?[)D]";

        int count = arr.Count(q => Regex.IsMatch(q, pattern));

        return count;
    }
}
